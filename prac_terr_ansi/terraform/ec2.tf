# 数値演算用の計算機作成（チュートリアル）

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_instance" "math-machine" {
  ami                    = "ami-09b68f5653871885f" # ubuntu18.04
  instance_type          = "t2.micro"
  key_name               = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = [
    "${aws_security_group.math-machine.id}"
  ]
  tags                   = {
    Name  = "math-machine"
    Users = "haga"
  }
}

resource "aws_key_pair" "auth" {
  key_name = "${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}
